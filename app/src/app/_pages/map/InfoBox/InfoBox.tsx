import './InfoBox.css';

export function InfoBox({lat, lng, zoom}: {lat: Number, lng: Number, zoom: Number}) {

  return (
    <div className="map-infobox" title="Lat : Lng : Zoom">
      {lat} : {lng} : {zoom}
    </div>
  )
}