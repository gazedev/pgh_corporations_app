import React from 'react';
import { render, screen } from '@testing-library/react';
import { App } from './App';

test('renders App link', () => {
  render(<App />);
  const linkElement = screen.getByText(/Corporations/i);
  expect(linkElement).toBeInTheDocument();
});
