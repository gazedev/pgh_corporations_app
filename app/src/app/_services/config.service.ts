class ConfigService {
  get(value: string): string | undefined {
    return process.env[`REACT_APP_${value}`];
  }
}

export default new ConfigService();
