# Pgh Corporations Map App

```
cp variables.env.example variables.env
docker-compose up
```
Paste your mapbox token into the variables.env file

Open http://localhost:3001 in your browser

To run yarn/npm commands, while the container is running:

```
docker-compose exec app bash
```

That will give you a bash terminal inside the container in the app directory.

## Linking

The map lives at /map

You can link to a specific view with ?lat={number}&long={number}&zoom={number}

You can link to a corporations with ?corpId={corporationId}&coprId={corporationId}